define(function(require) {
     var v1 = require('common/comment-message');
     var v2 = require('common/comment-reply');
     var v3 = require('common/common-comment');
     var v4 = require('common/common-seeUser');
     var v5 = require('common/common-seeUserBlog');
     var v6 = require('common/index');
     return {
         'comment-message': v1,
         'comment-reply': v2,
         'common-comment': v3,
         'common-seeUser': v4,
         'common-seeUserBlog': v5,
         'index': v6
      };
});