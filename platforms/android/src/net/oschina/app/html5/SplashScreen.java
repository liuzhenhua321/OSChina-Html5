package net.oschina.app.html5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import net.oschina.app.html5.R;

public class SplashScreen extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.page_splash_screen);
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				Intent intent=new Intent(SplashScreen.this, MainActivity.class);
				startActivity(intent);
				finish();
			}
		}, 2000);
	}
}
