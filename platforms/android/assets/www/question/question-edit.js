define(['text!question/question-edit.html', "base/openapi", 'base/util', "../base/login/login"],
	function(viewTemplate, OpenAPI, Util, Login) {
		return Piece.View.extend({
			id: 'question_question-edit',
			login: null,
			render: function() {
				login = new Login();
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},

			events: {
				"click .homeBtn": "goBackToHome",
				"click .publish": "publishQuestionEditContent",
				"focus .titleEdit": "titleFocus"
				// "focus .editTextarea": "contentFocus"
			},
			titleFocus: function() {
				if (navigator.userAgent.indexOf("MI") > -1) {
					$(".scrollContent").attr("style", "position:fixed !important;");

				}

			},
			// contentFocus: function() {
			// 	$(".scrollContent").attr("style","position:absolute !important;");
			// },

			goBackToHome: function() {
				window.history.back();
			},

			publishQuestionEditContent: function() {
				var me = this;

				var title = $('.titleEdit').val();
				var classify = $('#editClassify').val();
				var textareaVal = $('.editTextarea').val();
				var isNotice = $('.editNotice').is(':checked');
				var catalog = $("#editClassify").val();
				console.info("====================");
				console.info(catalog);
				titleNew = title.replace(/(\s*$)/g, ""); //去掉右边空格
				textareaValNew = textareaVal.replace(/(\s*$)/g, ""); //去掉右边空格


				var checkLogin = Util.checkLogin();

				if (checkLogin === false) {
					login.show();
				} else {
					if (titleNew === "" || titleNew === " ") {
						new Piece.Toast("请输入标题");
					} else if (textareaValNew === "" || textareaValNew === " ") {
						new Piece.Toast("请输入提问内容");
					} else {
						var user_token = Piece.Store.loadObject("user_token");
						var access_token = user_token.access_token;
						Util.Ajax(OpenAPI.post_pub, "GET", {
							"access_token": access_token,
							"catalog": catalog,
							"title": title,
							"content": textareaVal,
							"dataType": "jsonp"
							/*"isNoticeMe":isNotice,*/
						}, 'json', function(data, textStatus, jqXHR) {
							if (data.error === "200") {
								new Piece.Toast('发帖成功');
								setTimeout(function() {
									history.back();
								}, 1500);
							} else {
								new Piece.Toast(data.error_description);
							}

						}, null, null);
					}

				}
			},

			onShow: function() {
				var me = this;
				$(".titleEdit").focus();

				$(".titleEdit").blur(function() {
					$(".scrollContent").attr("style", "position:absolute !important;");
				});

				// var myScroll = new iScroll("iscrollContent", {
				// 		checkDOMChanges: true
				// 	});
				// $(window).resize(function() {
				// 	$('.editTextarea').live('focus',function(){
				// 		me.showkeyboard();
				// 	})

				// });
			}
		}); //view define

	});